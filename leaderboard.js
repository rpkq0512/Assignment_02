leaderboardState = {
  create: function() {

    // TITLE
    game.add.text(30, 30, 'LEADERBOARD', { font: '50px Arial', fill: '#ffffff' });
    game.add.text(30, game.height - 50, 'Press B to back to menu', { font: '20px Arial', fill: '#ffffff' });

    // DATA
    var num = 0;
    firebase.database().ref('ranking').orderByChild('score').limitToLast(3).on('child_added', snap => {
      game.add.text(100, 300 - num++*80, `Name: ${snap.val().name}   Score: ${snap.val().score}`, { font: '40px Arial', fill: '#ffffff' });
    });

    // INPUT
    this.keyB = game.input.keyboard.addKey(Phaser.Keyboard.B);
    this.keyB.onDown.add(this.backToMenu);
  },
  backToMenu: function() {
    game.state.start('menu');
  }
}