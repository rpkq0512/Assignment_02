# Assignment_02
* Please open the game with firefox 

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete Game Process|15%|Y|
|Basic Rules|20%|Y|
|Jucify Mechanism|15%|Y|
|Animation|10%|Y|
|Particle System|10%|Y|
|UI|5%|Y|
|Sound Effect|5%|Y|
|Leaderboard|5%|Y|
|Special Bullet|5%|Y|


## Component Description
### Jucify
[Level] : when acheive 300 points in level1, level 2 starts automatically (press CTRL to acheive 300 points)
[UltimateSkill] : press C to use special attack when the `special attack` value acheive 5.

### Volume
press W and S to change volume

### Bonus
[Item] : Eat yellow item to use special bullets. Eat pink item to heal.



## How to play
### Menu 
press key UP to start game, key DOWN to see leader board

### Leaderboard
press key B to back to menu

### Game
Use cursors to control player movement, SPACE to attack, key C to use special attack.
When score acheive 300 points, level 2 starts (press key CONTROL to acheive 300 points immediately).
Eat yellow item to get special bullet for 5 seconds.
Eat pink item to heal.

### Gameover
Enter player name and press key ENTER to add your name to the leaderboard.
press CTRL + C to back to menu
press CTRL + Q to quit game




