// ENEMY
var Enemy = function(pgame, enemyBullets, timer) {

  pgame.physics.enable(this, Phaser.Physics.ARCADE);
  this.anchor.setTo(0.5, 0.5);
  this.checkWorldBounds = true;
  this.outOfBoundsKill = true;
  this.timer = timer;

  this.hurt = function() {
    this.Health--;
    if(this.Health <= 0) this.kill();
    return (this.Health <= 0);
  }

  this.addEnemyBullet = function(x, y, speedX, speedY) {
    var bullet = enemyBullets.getFirstDead();
    if (!bullet) {
      bullet = new EnemyBullet(pgame, this.timer);
      enemyBullets.add(bullet);
    }
    bullet.anchor.setTo(0.5, 0.5);
    bullet.reset(x, y);
    bullet.velocityX = speedX;
    bullet.velocityY = speedY;
  };
  this.velocityX = 0;
  this.velocityY = 0;
  this.checkPause = function() {
    if(this.timer.paused) {
      this.body.velocity.x = 0;
      this.body.velocity.y = 0;
    }
    else {
      this.body.velocity.x = this.velocityX;
      this.body.velocity.y = this.velocityY;
    }
  }
};

// TANK
var Tank = function(pgame, enemyBullets, timer) {

  Phaser.Sprite.call(this, pgame, 0, 0, 'enemyTank');
  Enemy.call(this, pgame, enemyBullets, timer);
  this.animations.add('idle', [0, 1], 4, true);

  this.attackEvent = this.timer.loop(1500, function() {
    if(this.alive)
      this.attack(this);
  }, this);

  this.attack = function() {
    const maxBullets = 20;
    const bulletSpeed = 300;
    for(let i=0; i<maxBullets; i++) {
      let speedX = bulletSpeed * Math.cos(2 * Math.PI / maxBullets*i);
      let speedY = bulletSpeed * Math.sin(2 * Math.PI / maxBullets*i);
      this.addEnemyBullet(this.position.x, this.position.y, speedX, speedY);
    }
  }
};
Tank.prototype = Object.create(Phaser.Sprite.prototype);
Tank.prototype.constructor = Tank;
Tank.prototype.update = function() {
  this.checkPause();
}


// PLANE
var Plane = function(pgame, enemyBullets, player, timer) {

  Phaser.Sprite.call(this, pgame, 0, 0, 'enemyPlane');
  Enemy.call(this, pgame, enemyBullets, timer);
  this.animations.add('idle', [0, 1], 4, true);

  this.attackEvent = this.timer.loop(2000, function() {
    if(this.alive)
      this.attack(this);
  }, this);

  this.attack = function() {
    const maxBullets = 5;
    const bulletTimeInterval = 100;
    const bulletSpeed = 300;
    for(let i=0; i<maxBullets; i++) {
      game.time.events.add(i*bulletTimeInterval, function() {
        let dist = Phaser.Math.distance(this.position.x, this.position.y, player.position.x, player.position.y);
        let speedX = bulletSpeed * (player.position.x - this.position.x) / dist;
        let speedY = bulletSpeed * (player.position.y - this.position.y) / dist;
        this.addEnemyBullet(this.position.x, this.position.y, speedX, speedY);
      }, this);
    }
  }
};
Plane.prototype = Object.create(Phaser.Sprite.prototype);
Plane.prototype.constructor = Plane;
Plane.prototype.update = function() {
  if(this.position.x < 40 && this.velocityX < 0) this.velocityX *= -1; 
  else if(game.width - 40 < this.position.x &&  0 < this.velocityX) this.velocityX *= -1; 
  
  this.checkPause();
}

var EnemyBullet = function(pgame, timer) {
  Phaser.Sprite.call(this, pgame, 0, 0, 'enemy_bullet');
  pgame.physics.enable(this, Phaser.Physics.ARCADE);
  this.anchor.setTo(0.5, 0.5);
  this.checkWorldBounds = true;
  this.outOfBoundsKill = true;
  this.timer = timer;

  this.velocityX = 0;
  this.velocityY = 0;

  this.checkPause = function() {
    if(this.timer.paused) {
      this.body.velocity.x = 0;
      this.body.velocity.y = 0;
    }
    else {
      this.body.velocity.x = this.velocityX;
      this.body.velocity.y = this.velocityY;
    }
  }
}
EnemyBullet.prototype = Object.create(Phaser.Sprite.prototype);
EnemyBullet.prototype.constructor = EnemyBullet;
EnemyBullet.prototype.update = function() {
  this.checkPause();
}
