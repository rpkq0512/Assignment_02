////// PLAYER ///////
var Player = function(pgame, playerBullets, specialAttack, shootSound, bombSound, timer) {
  
  Phaser.Sprite.call(this, pgame, 500, 300, 'player');
	this.anchor.setTo(0.5, 0.5);
  pgame.physics.enable(this, Phaser.Physics.ARCADE);
  this.body.collideWorldBounds = true;
  this.timer = timer;
  
  // INIT
  this.animations.add('idle', [0, 2], 4, true);
  this.animations.add('walk', [0, 1], 4, true);

  // INPUT
  this.cursor = game.input.keyboard.createCursorKeys();
  this.keySpace = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
  this.keyC = game.input.keyboard.addKey(Phaser.Keyboard.C);
  this.keyControl = game.input.keyboard.addKey(Phaser.Keyboard.CONTROL);

  // PARTICLE
  this.emitter = game.add.emitter(0, 0, 15);
  this.emitter.makeParticles('pixel');
  this.emitter.setYSpeed(-150, 150);
  this.emitter.setXSpeed(-150, 150);
  this.emitter.setScale(2, 0, 2, 0, 800);
  this.emitter.gravity = 0;
  
  this.Health = 10;
  this.hurt = function() {
    this.Health--;
    if(this.Health <= 0) this.die();
    game.camera.shake(0.005, 150);
  };

  this.heal = function() {
    this.Health += 2;
  };

  this.move = function() {
    // LEFT RIGHT
    if (this.cursor.left.isDown) {
      this.body.velocity.x = -200;
      this.animations.play('walk'); 
    }
    else if (this.cursor.right.isDown) {
      this.body.velocity.x = 200;
      this.animations.play('walk'); 
    }
    else {
      this.body.velocity.x = 0;
    } 

    // UP DOWN
    if (this.cursor.up.isDown) {
      this.body.velocity.y = -200;
      this.animations.play('walk'); 
    }
    else if (this.cursor.down.isDown) {
      this.body.velocity.y = 200;
      this.animations.play('walk'); 
    }
    else {
      this.body.velocity.y = 0;
    }
    if(!this.cursor.left.isDown && !this.cursor.right.isDown && !this.cursor.up.isDown && !this.cursor.down.isDown) {
      this.animations.play('idle'); 
    }
  };

  this.playerBulletEnable = true;
  this.specialBulletEnable = false;
  this.attack = function() {
    // NORMAL ATTACK
    if(this.keySpace.isDown && this.playerBulletEnable) {
      // SHOOT
      var bullet = playerBullets.getFirstDead();
      if (!bullet) {
        bullet = new PlayerBullet(pgame, this.timer);
        playerBullets.add(bullet);
      }
      bullet.reset(this.position.x, this.position.y);
      bullet.velocityY = -300;
      bullet.velocityX = 0;
      
      if(this.specialBulletEnable) {
        var bullet2 = playerBullets.getFirstDead();
        if (!bullet2) {
          bullet2 = new PlayerBullet(pgame, this.timer);
          playerBullets.add(bullet2);
        }
        bullet2.reset(this.position.x, this.position.y);
        bullet2.velocityY = -300*Math.sin(1.3);
        bullet2.velocityX = 300*Math.cos(1.3);

        var bullet3 = playerBullets.getFirstDead();
        if (!bullet3) {
          bullet3 = new PlayerBullet(pgame, this.timer);
          playerBullets.add(bullet3);
        }
        bullet3.reset(this.position.x, this.position.y);
        bullet3.velocityY = -300*Math.sin(1.3);
        bullet3.velocityX = -300*Math.cos(1.3);
      }
      
      shootSound.play();
      // TIMER
      this.playerBulletEnable = false;
      game.time.events.add(Phaser.Timer.SECOND * 0.1, function() { this.playerBulletEnable = true; }, this);
    }

    // SPECIAL ATTACK
    if(this.keyC.isDown && specialAttack.canAttack()) {
      specialAttack.attack(this.position);
      bombSound.play();
    }
  };

  this.die = function() {
    this.kill();

    // PARTICLE SYSTEM
    this.emitter.x = this.position.x;
    this.emitter.y = this.position.y;
    this.emitter.start(true, 800, null, 15);

    // CAMERA EFFECT
    game.camera.flash(0xffffff, 300);
    

    game.time.events.add(1000, function() {
      game.state.start('gameover');
    }, this);
  };

};
Player.prototype = Object.create(Phaser.Sprite.prototype);
Player.prototype.constructor = Player;
Player.prototype.update = function() {
  if(this.alive && !this.timer.paused) {
    this.move();
    this.attack();
  }
  else {
    this.body.velocity.x = 0;
    this.body.velocity.y = 0;
  }
  //if(this.keyControl.isDown) this.die();
};


/////// SPECIAL ATTACK ///////
var SpecialAttack = function(pgame) {
  Phaser.Sprite.call(this, pgame, -50, -50, 'player_specialAttack');
	this.anchor.setTo(0.5, 1);
  pgame.physics.enable(this, Phaser.Physics.ARCADE);

  this.timerMax = 5;
  this.timerVal = 0;

  this.canAttack = function() {
    if(this.timerVal === this.timerMax) return true;
    return false;
  };

  this.attack = function(playerPos) {
    this.timerVal = 0;
    this.position.x = playerPos.x;
    this.position.y = playerPos.y;
    game.time.events.add(Phaser.Timer.SECOND * 0.5, function() {
      this.position.x = -50;
      this.position.y = -50;
    }, this);
  }

  game.time.events.loop(3000, function() {
     if(this.timerVal < this.timerMax) this.timerVal ++;
  }, this);

};
SpecialAttack.prototype = Object.create(Phaser.Sprite.prototype);
SpecialAttack.prototype.constructor = SpecialAttack;


/////// BULLET ///////
var PlayerBullet = function(pgame, timer) {
  Phaser.Sprite.call(this, pgame, 0, 0, 'player_bullet');
  pgame.physics.enable(this, Phaser.Physics.ARCADE);
  this.anchor.setTo(0.5, 0.5);
  this.checkWorldBounds = true;
  this.outOfBoundsKill = true;
  this.timer = timer;

  this.velocityX = 0;
  this.velocityY = 0;

  this.checkPause = function() {
    if(this.timer.paused) {
      this.body.velocity.x = 0;
      this.body.velocity.y = 0;
    }
    else {
      this.body.velocity.x = this.velocityX;
      this.body.velocity.y = this.velocityY;
    }
  }
}
PlayerBullet.prototype = Object.create(Phaser.Sprite.prototype);
PlayerBullet.prototype.constructor = PlayerBullet;
PlayerBullet.prototype.update = function() {
  this.checkPause();
}
