var level2_State = {
  create: function() {
    game.physics.startSystem(Phaser.Physics.ARCADE);
    
    // CONSTANTS
    this.mapSrcollSpeed = 80;
    this.maxPlayerBullets = 20;
    this.maxEnemyBullets = 100;

    
    // MAP 
    this.starBackground = game.add.tileSprite(0, 0, game.width, game.height, "starBackground");;
    game.physics.enable(this.starBackground, Phaser.Physics.ARCADE);
    this.starBackground.autoScroll(0, this.mapSrcollSpeed);
    
    // SOUND
    this.shootSound = game.add.audio('shootSound');
    this.bombSound = game.add.audio('bombSound');
    this.bgm = game.add.audio('bgm');
    this.bgm.volume = this.bombSound.volume = this.shootSound.volume = 0.5;
    this.bgm.play();
    // SOUND flags
    this.ShasChanged = false;
    this.WhasChanged = false;
    
    // INPUT
    this.keyW = game.input.keyboard.addKey(Phaser.Keyboard.W);
    this.keyW.onDown.add(this.changeVolume, this);
    this.keyS = game.input.keyboard.addKey(Phaser.Keyboard.S);
    this.keyS.onDown.add(this.changeVolume, this);
    
    // PLAYER SPECIAL ATTACK
    this.specialAttack = new SpecialAttack(game);
    game.world.add(this.specialAttack);
    
    // PLAYER BULLET
    this.maxPlayerBullets = 50;
    this.playerBullets = game.add.group();
    this.playerBullets.enableBody = true;
    this.playerBullets.createMultiple(this.maxPlayerBullets, 'player_bullet');
    this.playerBulletEnable = true;
    
    // PLAYER
    this.player = new Player(game, this.playerBullets, this.specialAttack, this.shootSound, this.bombSound);
    game.world.add(this.player);

    // ENEMY
    game.time.events.loop(2300, this.addEnemy, this);

    // ENEMY TANK
    this.enemyTanks = game.add.group();
    this.enemyTanks.enableBody = true;
    this.enemyTanks.physicsBodyType = Phaser.Physics.ARCADE;

    // ENEMY PLANE
    this.enemyPlanes = game.add.group();
    this.enemyPlanes.enableBody = true;
    this.enemyTanks.physicsBodyType = Phaser.Physics.ARCADE;
    
    // ENEMY BULLET
    this.enemyBullets = game.add.group();
    this.enemyBullets.enableBody = true;
    this.enemyBullets.createMultiple(this.maxEnemyBullets, 'enemy_bullet');

    // COIN
    /*this.coin = game.add.sprite(60, 140, 'coin');
    game.physics.arcade.enable(this.coin);
    this.coin.anchor.setTo(0.5, 0.5);*/

    // UI
    this.scoreLabelUI = game.add.text(30, 30, 'score: 0', { font: '18px Arial', fill: '#ffffff' });
    this.specialAttackBarUI = game.add.text(30, 60, 'special Attack: 0', { font: '18px Arial', fill: '#ffffff' });
    this.playerHealtUI = game.add.text(30, 90, 'health: 10', { font: '18px Arial', fill: '#ffffff' });
    this.volumeUI = game.add.text(30, 120, 'volume: 100', { font: '18px Arial', fill: '#ffffff' });

  },
  update: function() {
    // COLLISION
    game.physics.arcade.overlap(this.player, this.enemyBullets, function(player, bullet) { 
      player.hurt();
      bullet.kill();
    }, null, this);
    game.physics.arcade.overlap(this.enemyTanks, this.playerBullets, function(enemy, bullet) { 
      var isDead = enemy.hurt();
      if(isDead) game.global.score += 10;
      bullet.kill();
    }, null, this);
    game.physics.arcade.overlap(this.enemyPlanes, this.playerBullets, function(enemy, bullet) { 
      var isDead = enemy.hurt();
      if(isDead) game.global.score += 30;
      bullet.kill();
    }, null, this);
    game.physics.arcade.overlap(this.enemyTanks, this.specialAttack, function(bullet, enemy) {
      enemy.hurt();
    }, null, this);
    game.physics.arcade.overlap(this.enemyPlanes, this.specialAttack, function(bullet, enemy) {
      enemy.hurt();
    }, null, this);

    this.displayUI();
    //if (!this.player.inWorld) { this.playerDie();}
  }, // No changes
  takeCoin: function(player, coin) {
    // SCORE
    /*game.global.score += 5;
    
    // TWEEN
    this.coin.scale.setTo(0, 0);
    game.add.tween(this.coin.scale).to({x: 1, y: 1}, 300).start();
    game.add.tween(this.player.scale).to({x: 1.5, y: 1.5}, 100).yoyo(true).start();*/
  },
  displayUI: function() {
    this.scoreLabelUI.text = 'score: ' + game.global.score;
    this.specialAttackBarUI.text = 'special attack: ' + this.specialAttack.timerVal;
    this.playerHealtUI.text = 'health: ' + this.player.Health;
    this.volumeUI.text = 'volume: ' + this.bgm.volume;
  },
  changeVolume: function(e) {
    if(e.event.key == 'w') val = 0.1;
    else if(e.event.key == 's') val = -0.1;

    var volume = this.bgm.volume;
    volume += val;
    if(volume < 0) volume = 0;
    else if(1 < volume) volume = 1;
    volume = Math.round(volume * 10)/10;
    this.bgm.volume = volume;
    this.shootSound.volume = volume;
    this.bombSound.volume = volume;
  },
  addEnemy: function() {
    var type = game.rnd.pick(['tank', 'plane']);
    switch(type){
      case 'tank':
        var tank = this.enemyTanks.getFirstDead();
        if(!tank) {
          tank = new Tank(game, this.enemyBullets);
          this.enemyTanks.add(tank);
        }
        var positionX = game.rnd.integerInRange(40, game.width - 40);
        tank.reset(positionX, 0);
        tank.body.velocity.y = this.mapSrcollSpeed;
        tank.Health = 5;
      break;
      case 'plane':
        var plane = this.enemyPlanes.getFirstDead();
        if(!plane) {
          plane = new Plane(game, this.enemyBullets, this.player);
          this.enemyPlanes.add(plane);
        }
        var positionX = game.rnd.integerInRange(40, game.width - 40);
        plane.reset(positionX, 0);
        plane.body.velocityY = this.mapSrcollSpeed;
        plane.body.velocityX = 100;
        plane.Health = 3;
      break;
    }
  }
};


