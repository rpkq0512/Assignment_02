var menuState = {
  create: function() {
    game.add.text(100, 100, 'Raiden', { font: '50px Arial', fill: '#ffffff' });
    game.add.text(100, 200, 'press the up arrow key to start', { font: '20px Arial', fill: '#ffffff' });
    game.add.text(100, 230, 'press the down arrow key to see leaderboard', { font: '20px Arial', fill: '#ffffff' });
    game.add.text(100, 260, 'press S or W to change volume in game', { font: '20px Arial', fill: '#ffffff' });

    var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
    upKey.onDown.add(this.start, this);

    var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
    downKey.onDown.add(this.openLeaderboard, this);
  },
  start: function() {
    // Start the actual game
    game.state.start('level1');
  },
  openLeaderboard: function() {
    game.state.start('leaderboard');
  }
}; 