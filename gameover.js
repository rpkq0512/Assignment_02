var gameoverState = {
  create: function() {
    
    // INPUT PLUGIN
    game.add.plugin(PhaserInput.Plugin);
    this.nameInput = game.add.inputField(game.width/2 - 130, game.height/2 - 50, {
      font: '18px Arial',
      fill: '#212121',
      fontWeight: 'bold',
      width: 150,
      padding: 8,
      borderWidth: 1,
      borderColor: '#000',
      borderRadius: 6,
      placeHolder: 'NAME',
      type: PhaserInput.InputType.text
    });
    this.nameInput.startFocus();

    // TEXT
    this.text = [];
    this.text.push(game.add.text(game.width/2 - 130, game.height/2, `score: ${game.global.score}`, { font: '18px Arial', fill: '#ffffff' }));
    this.text.push(game.add.text(game.width/2 - 130, game.height/2 + 30, 'press ENTER after enter your name', { font: '18px Arial', fill: '#ffffff' }));
    this.text.push(game.add.text(game.width/2 - 130, game.height/2 + 60, 'press CTRL + C to continue', { font: '18px Arial', fill: '#ffffff' }));
    this.text.push(game.add.text(game.width/2 - 130, game.height/2 + 90, 'press CTRL + Q to quit', { font: '18px Arial', fill: '#ffffff' }));

    // INPUT
    this.keyQ = game.input.keyboard.addKey(Phaser.Keyboard.Q);
    this.keyC = game.input.keyboard.addKey(Phaser.Keyboard.C);
    this.keyControl = game.input.keyboard.addKey(Phaser.Keyboard.CONTROL);
    this.keyEnter = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);

  },
  update: function() {
    if(this.keyQ.isDown && this.keyControl.isDown) this.quitGame();
    if(this.keyC.isDown && this.keyControl.isDown) game.state.start('menu'); 
    this.addRanking();
  },
  addRanking: function() {
    if(this.keyEnter.isDown && !this.enterFlag) {
      firebase.database().ref("/ranking/").push({
        name: this.nameInput.text._text,
        score: game.global.score
      });
      alert("ranking added!")
      this.nameInput.destroy();
    }
  },
  quitGame: function() {
    game.state.start('quit');
  }
}