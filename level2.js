var level2_State = {
  create: function() {
    game.physics.startSystem(Phaser.Physics.ARCADE);
    
    // CONSTANTS
    this.mapSrcollSpeed = 30;
    this.maxPlayerBullets = 20;
    this.maxEnemyBullets = 100;
    
    // EVENTS
    this.game_timer = game.time.create(false);
    this.game_timer.loop(2500, this.addEnemy, this);
    this.game_timer.loop(20000, this.addItem, this);
    this.game_timer.start();
    
    // MAP 
    this.starBackground = game.add.tileSprite(0, 0, game.width, game.height, "starBackground");;
    game.physics.enable(this.starBackground, Phaser.Physics.ARCADE);
    this.starBackground.autoScroll(0, this.mapSrcollSpeed);

    // ITEM
    this.item = game.add.sprite(-5, -5, 'item');
    game.physics.enable(this.item, Phaser.Physics.ARCADE);
    this.item.kill();
    this.healItem = game.add.sprite(-5, -5, 'healItem');
    game.physics.enable(this.healItem, Phaser.Physics.ARCADE);
    this.healItem.kill();
    
    // SOUND
    this.shootSound = game.add.audio('shootSound');
    this.bombSound = game.add.audio('bombSound');
    this.bgm = game.add.audio('bgm');
    this.bgm.volume = this.bombSound.volume = this.shootSound.volume = 0;
    this.bgm.play();
    // SOUND flags
    this.ShasChanged = false;
    this.WhasChanged = false;
    
    
    // PLAYER SPECIAL ATTACK
    this.specialAttack = new SpecialAttack(game);
    game.world.add(this.specialAttack);
    
    // PLAYER BULLET
    this.playerBullets = game.add.group();
    this.playerBullets.enableBody = true;
    
    // PLAYER
    this.player = new Player(game, this.playerBullets, this.specialAttack, this.shootSound, this.bombSound, this.game_timer);
    game.world.add(this.player);

    // ENEMY TANK
    this.enemyTanks = game.add.group();
    this.enemyTanks.enableBody = true;
    this.enemyTanks.physicsBodyType = Phaser.Physics.ARCADE;

    // ENEMY PLANE
    this.enemyPlanes = game.add.group();
    this.enemyPlanes.enableBody = true;
    this.enemyPlanes.physicsBodyType = Phaser.Physics.ARCADE;
    
    // ENEMY BULLET
    this.enemyBullets = game.add.group();
    this.enemyBullets.enableBody = true;
    
    // UI
    this.scoreLabelUI = game.add.text(30, 30, 'score: 0', { font: '18px Arial', fill: '#ffffff' });
    this.specialAttackBarUI = game.add.text(30, 60, 'special Attack: 0', { font: '18px Arial', fill: '#ffffff' });
    this.playerHealtUI = game.add.text(30, 90, 'health: 10', { font: '18px Arial', fill: '#ffffff' });
    this.volumeUI = game.add.text(30, 120, 'volume: 100', { font: '18px Arial', fill: '#ffffff' });

    // GAMEOVER
    this.gameoverLabel = game.add.text(game.width/2, -50, 'Next LEVEL', { font: '50px Arial', fill: '#ffffff' });
    this.gameoverLabel.anchor.setTo(0.5, 0.5);
    this.gameoverTween = game.add.tween(this.gameoverLabel).to({y: game.height/2}, 1000).easing(Phaser.Easing.Bounce.Out);
    this.gameover = false;
    
    // INPUT
    this.keyW = game.input.keyboard.addKey(Phaser.Keyboard.W);
    this.keyW.onDown.add(this.changeVolume, this);
    this.keyS = game.input.keyboard.addKey(Phaser.Keyboard.S);
    this.keyS.onDown.add(this.changeVolume, this);
    this.keyControl = game.input.keyboard.addKey(Phaser.Keyboard.CONTROL);
    this.keyControl.onDown.add(function() { game.global.score = 300; });
    this.keyP = game.input.keyboard.addKey(Phaser.Keyboard.P);
    this.keyP.onDown.add(this.pauseGame, this);

    // PARTICLE SYSTEM
    this.playerEmitters = game.add.group();
    this.enemyEmitters = game.add.group();
  },
  update: function() {
    // COLLISION
    game.physics.arcade.overlap(this.player, this.enemyBullets, function(player, bullet) {
      this.emitEnemyParticle(bullet.position.x, bullet.position.y);
      player.hurt();
      bullet.kill();
    }, null, this);

    // ENEMY
    game.physics.arcade.overlap(this.enemyTanks, this.playerBullets, function(enemy, bullet) {
      this.emitPlayerParticle(bullet.position.x, bullet.position.y);
      var isDead = enemy.hurt();
      if(isDead) game.global.score += 30;
      bullet.kill();
    }, null, this);
    game.physics.arcade.overlap(this.enemyTanks, this.specialAttack, function(bullet, enemy) {
      this.emitPlayerParticle(bullet.position.x, bullet.position.y);
      var isDead = enemy.hurt();
      if(isDead) game.global.score += 30;
    }, null, this);
    game.physics.arcade.overlap(this.enemyPlanes, this.playerBullets, function(enemy, bullet) {
      this.emitPlayerParticle(bullet.position.x, bullet.position.y);
      var isDead = enemy.hurt();
      if(isDead) game.global.score += 50;
      bullet.kill();
    }, null, this);
    game.physics.arcade.overlap(this.enemyPlanes, this.specialAttack, function(bullet, enemy) {
      this.emitPlayerParticle(bullet.position.x, bullet.position.y);
      var isDead = enemy.hurt();
      if(isDead) game.global.score += 50;
    }, null, this);

    // ITEM
    game.physics.arcade.overlap(this.item, this.player, function() {
      this.item.kill();
      this.player.specialBulletEnable = true;
      game.time.events.add(5000, function() { this.player.specialBulletEnable = false; }, this);
    }, null, this);
    game.physics.arcade.overlap(this.healItem, this.player, function() {
      this.healItem.kill();
      this.player.heal();
    }, null, this);

    this.displayUI();
  }, 
  displayUI: function() {
    this.scoreLabelUI.text = 'score: ' + game.global.score;
    this.specialAttackBarUI.text = 'special attack: ' + this.specialAttack.timerVal;
    this.playerHealtUI.text = 'health: ' + this.player.Health;
    this.volumeUI.text = 'volume: ' + this.bgm.volume;
  },
  changeVolume: function(e) {
    if(e.event.key == 'w') val = 0.1;
    else if(e.event.key == 's') val = -0.1;

    var volume = this.bgm.volume;
    volume += val;
    if(volume < 0) volume = 0;
    else if(1 < volume) volume = 1;
    volume = Math.round(volume * 10)/10;
    this.bgm.volume = volume;
    this.shootSound.volume = volume;
    this.bombSound.volume = volume;
  },
  addEnemy: function() {
    var type = game.rnd.pick(['tank', 'plane']);
    switch(type){
      case 'tank':
        var tank = this.enemyTanks.getFirstDead();
        if(!tank) {
          tank = new Tank(game, this.enemyBullets, this.game_timer);
          tank.animations.play('idle');
          this.enemyTanks.add(tank);
        }
        var positionX = game.rnd.integerInRange(40, game.width - 40);
        tank.reset(positionX, 0);
        tank.velocityY = this.mapSrcollSpeed;
        tank.Health = 3;
      break;
      case 'plane':
        var plane = this.enemyPlanes.getFirstDead();
        if(!plane) {
          plane = new Plane(game, this.enemyBullets, this.player, this.game_timer);
          plane.animations.play('idle');
          this.enemyPlanes.add(plane);
        }
        var positionX = game.rnd.integerInRange(40, game.width - 40);
        plane.reset(positionX, 0);
        plane.velocityY = this.mapSrcollSpeed;
        plane.velocityX = 100;
        plane.Health = 5;
      break;
    }
  },
  addItem: function() {
    var type = game.rnd.pick(['item', 'healItem']);
    switch(type){
      case 'item':
        if(!this.item.alive) {
          var positionX = game.rnd.integerInRange(40, game.width - 40);
          var positionY = game.rnd.integerInRange(40, game.height - 40);
          this.item.reset(positionX, positionY);
        }
      break;
      case 'healItem':
        if(!this.healItem.alive) {
          var positionX = game.rnd.integerInRange(40, game.width - 40);
          var positionY = game.rnd.integerInRange(40, game.height - 40);
          this.healItem.reset(positionX, positionY);
        }
      break;
    }
  },
  emitPlayerParticle: function(x, y) {
    var emitter = this.playerEmitters.getFirstDead();
    if(!emitter) {
      emitter = game.add.emitter(0, 0, 2);
      emitter.makeParticles('pixel');
      emitter.setYSpeed(-150, 150);
      emitter.setXSpeed(-150, 150);
      emitter.setScale(1.5, 0, 1.5, 0, 800);
      emitter.gravity = 0;
      this.playerEmitters.add(emitter);
    }
    emitter.x = x;
    emitter.y = y;
    emitter.start(true, 800, null, 15);
  },
  emitEnemyParticle: function(x, y) {
    var emitter = this.enemyEmitters.getFirstDead();
    if(!emitter) {
      emitter = game.add.emitter(0, 0, 4);
      emitter.makeParticles('enemy_pixel');
      emitter.setYSpeed(-150, 150);
      emitter.setXSpeed(-150, 150);
      emitter.setScale(1.8, 0, 1.8, 0, 800);
      emitter.gravity = 0;
      this.enemyEmitters.add(emitter);
    }
    emitter.x = x;
    emitter.y = y;
    emitter.start(true, 800, null, 15);
  },
  pauseGame: function() {
    if(this.game_timer.paused) {
      this.game_timer.resume();
      this.starBackground.autoScroll(0, this.mapSrcollSpeed);
    } 
    else {
      this.game_timer.pause();
      this.starBackground.stopScroll();
    }
  }
};

