var loadState = {
  preload: function () {
    // Add a 'loading...' label on the screen
    var loadingLabel = game.add.text(game.width/2, 150,
    'loading...', { font: '30px Arial', fill: '#ffffff' });
    loadingLabel.anchor.setTo(0.5, 0.5);

    // Load all game assets
    game.load.spritesheet('player', 'assets/player.png', 20, 20);
    game.load.image('item', 'assets/item.png');
    game.load.image('healItem', 'assets/healItem.png');

    game.load.image('starBackground', 'assets/starBackground.jpg');
    game.load.image('pixel', 'assets/pixel.png');
    game.load.image('enemy_pixel', 'assets/enemy_pixel.png');
    game.load.audio('shootSound', 'assets/shootSound.ogg');
    game.load.audio('bombSound', 'assets/bombSound.ogg');
    game.load.audio('bgm', 'assets/bgm.ogg');
    
    game.load.image('player_bullet', 'assets/player_bullet.png');
    game.load.image('player_specialAttack', 'assets/player_specialAttack.png');
    
    game.load.spritesheet('enemyTank', 'assets/enemyTank.png', 45, 40);
    game.load.spritesheet('enemyPlane', 'assets/enemyPlane.png', 45, 40);
    game.load.image('enemy_bullet', 'assets/enemy_bullet.png');
  },
  create: function() {
    // Go to the menu state
    game.state.start('menu');
  }
}; 