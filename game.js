// Initialize Phaser
var game = new Phaser.Game(1000, 600, Phaser.AUTO, 'canvas');
// Define our global variable
game.global = { score: 0 };
// Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('level1', level1_State);
game.state.add('level2', level2_State);
game.state.add('gameover', gameoverState);
game.state.add('quit', quitState);
game.state.add('leaderboard', leaderboardState);
// Start the 'boot' state
game.state.start('boot');